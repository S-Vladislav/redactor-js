<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="foundation/css/foundation.css">
    <script src="foundation/js/vendor/modernizr.js"></script>
    <link href="style.css" rel="stylesheet">
</head>

<body>

<div class="wrapper">

    <header class="header">
        <nav class="top-bar" data-topbar role="navigation">

            <ul class="title-area">
                <li class="name">
                    <h1><a href="#">Redactor JS Foundation</a></h1>
                </li>
                <li class="toggle-topbar menu-icon">
                    <a href="#"><span>Menu</span></a>
                </li>

            </ul>

        </nav>
    </header>
    <!-- .header-->
    <main class="content">

    <fieldset>
            <legend>Text Editor</legend>
            <div class="redactor-text"></div>
        </fieldset>

    </main>
    <!-- .content -->
</div>
<!-- .wrapper -->

<footer class="footer">
    <script type="text/javascript">
        function doStyle(style) {
        }

    </script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="foundation/js/foundation/foundation.js"></script>
    <script type="text/javascript" src="foundation/js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="foundation/js/foundation/foundation.reveal.js"></script>
    <script type="text/javascript" src="js/redactor.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('.redactor-text').redactor({

                class: 'editor-test'

                });

            $(".button").on("click", function () {

                var style = $(this).data('style');
                document.execCommand(style, false, null);
                return false;


            });

//            $('[contenteditable]').on('focus', function() {
//                var $this = $(this);
//                $this.data('before',focus $this.html());
//                return $this;
//            }).on('blur keyup paste', function() {
//                var $this = $(this);
//                if ($this.data('before') !== $this.html()) {
//                    $this.data('before', $this.html());
//                    $this.trigger('change');
//                }
//                return $this;
//            });


        });


    </script>

</footer>
<!-- .footer -->

</body>
</html>
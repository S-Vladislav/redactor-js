(function ($) {
    $.fn.redactor = function (options) {

        var format_insert = [];
        var format_header_insert = [];
        var list = [];
        var align = [];
        var more_options = [];
        var settings = $.extend({
            'class': 'default',
            'format_text': {'italic': 'italic', 'bold': 'bold', 'strike': 'strikeThrough'},
            'format_header': {'h1': 'h1', 'h2': 'h2', 'h3': 'h3', 'h4': 'h4', 'h5': 'h5', 'h6': 'h6'},
            'list': {'Number List': 'insertOrderedList', 'Point List': 'insertUnorderedList'},
            'align_text': {'Center': 'justifycenter', 'Full': 'justifyfull', 'Left': 'justifyleft','Right': 'justifyright'},
            'more_opt': {'Horizontal Line': 'insertHorizontalRule', 'Copy': '', 'Paste': 'paste','Cut': ''}
        }, options);


        $.each(settings.format_text, function (index, value) {

            var element = $('<li><a href="#" class="button command small" data-command="' + value + '"><i class="fa fa-' + value.toLowerCase() + '"></i></a></li>');

            format_insert.push(element[0]);
        });

        $.each(settings.format_header, function (index, value) {

            var element = $('<li><a href="#" class="command-format-header" data-format="formatBlock" data-command="' + value + '">' + index + '</a></li>');

            format_header_insert.push(element[0]);
        });
        $.each(settings.list, function (index, value) {

            var element = $('<li><a href="#" class="command" data-command="' + value + '">' + index + '</a></li>');

            list.push(element[0]);
        });
        $.each(settings.align_text, function (index, value) {

            var element = $('<li><a href="#" class="command" data-command="' + value + '">' + index + '</a></li>');

            align.push(element[0]);
        });
        $.each(settings.more_opt, function (index, value) {

            var element;
            if (value == 'paste') {
                element   = $('<li><a href="#" data-reveal-id="' + value + '" >' + index + '</a></li>');

            } else {
                 element = $('<li><a href="#" data-reveal-id="' + value + '" class="command" data-command="' + value + '">' + index + '</a></li>');

            }

            more_options.push(element[0]);
        });
        this.append($("<div class='container toolbar-one'>")
                .append($("<div class='row'>")
                    .append($("<div class='small-2 large-12 columns'>")
                        .append($("<div class='small-2 large-4 columns' id='format_box'>").append($("<ul class='button-group'>").append(format_insert)))
                        .append($("<div class='small-2 large-3 columns' id='header_box'>")
                            .append($("<a href='#' class='button small' data-dropdown='hover1' data-options='is_hover:true; hover_timeout:5000'>Header Style</a>"))
                            .append($("<ul id='hover1' class='f-dropdown' data-dropdown-content=''>").append(format_header_insert)))

                        .append($("<div class='small-2 large-3 columns' id='list_box' >")
                            .append($("<a href='#' class='button small' data-dropdown='hover2' data-options='is_hover:true; hover_timeout:5000'>List Style</a>"))
                            .append($("<ul id='hover2' class='f-dropdown' data-dropdown-content=''>").append(list)))
                        .append($("<div class='small-2 large-3 columns' id='align_box' >")
                            .append($("<a href='#' class='button small' data-dropdown='hover3' data-options='is_hover:true; hover_timeout:5000'>Alig Text</a>"))
                            .append($("<ul id='hover3' class='f-dropdown' data-dropdown-content=''>").append(align)))
                        .append("<div class='small-2 large-3 columns undo-redo'><ul class='button-group'>" +
                            "<li><a href='#'  class='button undo small'><i class='fa fa-undo'></i></a></li>" +
                            "<li><a href='#' class='button redo small'><i class='fa fa-repeat'></i></a></li></ul></div>")

                        .append("<div class='small-2 large-3 columns view_preview_box'><ul class='button-group'>" +
                            "<li><a href='#'  class='button view_text small'> <i class='fa fa-pencil-square-o'></i> Edit</a></li>" +
                            "<li><a href='#' class='button preview small'><i class='fa fa-eye'></i> Preview</a></li></ul></div>"))

                )).append($("<div class='container toolbar-two'>")
                .append($("<div class='row'>")
                    .append($("<div>").append("<div class='large-5 columns'><div class='row collapse'>" +
                        "<div class='small-10 large-7 columns'>" +
                        "<input  id='image_path' type='text'> </div>" +
                        "<div class='small-2 large-5 columns'><a href='#'  class='button insert_image small'> <i class='fa fa-picture-o'></i> Insert Image</a></div></div></div>"))

                    .append($("<div class='small-2 large-4 columns' >")
                        .append($("<a href='#' class='button small' data-dropdown='hover4' data-options='is_hover:true; hover_timeout:5000'> <i class='fa fa-chevron-circle-down'></i> More Options</a>"))
                        .append($("<ul id='hover4' class='f-dropdown' data-dropdown-content=''>").append(more_options)))
                )
            );
        var modal_paste = "<div id='paste' class='reveal-modal' data-reveal><h2>Insert Text.</h2><a class='close-reveal-modal'>&#215;</a></div>";

        var editor = "<div class='redactor-js'><div id=" + settings.class + " class=" + settings.class + " contenteditable='true' ></div>"+modal_paste+"</div>";


       this.append(editor);

        $(".command").on("click", function () {
            var command = $(this).data('command');
//            if (command=="paste") {
//              var text =  window.prompt("Copy to clipboard: Ctrl+C, Enter");
//             $('.'+settings.class).html(text);
//                return false;
//            }
            document.execCommand(command, false, false);
            return false;

        });
        $(".command-format-header").on("click", function () {
            var command = $(this).data('command');
            document.execCommand('formatBlock', false, command);
            return false;

        });

        $(".preview").on("click", function () {
            $("." + settings.class).show();
            $("textarea." + settings.class).remove();
            $("." + settings.class).focus();
            return false;
        });

        $(".undo").on("click", function () {
            document.execCommand('undo', false, null);
            return false;
        });

        $(".redo").on("click", function () {
            document.execCommand('redo', false, null);
            return false;
        });

        $(".view_text").on("click", function () {
            var divHtml = $("." + settings.class).html();
            var editableText = $("<textarea />").addClass(settings.class);
            $("." + settings.class).hide();
            editableText.val(divHtml);
            $('.redactor-js').append(editableText);
            editableText.focus();
            return false;
        });

        $(".insert_image").on("click", function () {
            document.execCommand("InsertImage", false, $('#image_path').val());
            $('#image_path').val("");

            return false;
        });

        function previousNode(node) {
            var previous = node.previousSibling;
            if (previous) {
                node = previous;
                while (node.hasChildNodes()) {
                    node = node.lastChild;
                }
                return node;
            }
            var parent = node.parentNode;
            if (parent && parent.nodeType.hasChildNodes) {
                return parent;
            }
            return null;
        }

        var sel = window.getSelection();
        if (sel.rangeCount > 0) {
            var range = sel.getRangeAt(0);
            var node = range.startContainer;
            if (node.hasChildNodes() && range.startOffset > 0) {
                node = node.childNodes[range.startOffset - 1];
            }

            while (node) {
                if (node.nodeType == 1 && node.tagName.toLowerCase() == "img") {
                    alert("Found inserted image with src " + node.src);
                    break;
                }
                node = previousNode(node);
            }
        }

        $(document).foundation({
            dropdown: {

                active_class: 'open'
            }
        });
    };
})(jQuery);


